import logo from './logo.svg';
import HelloMessage from './components/HelloMessage';
import './App.css';
import React from 'react';
import Timer from './components/Timer';
import BoutonNbClick from './components/BoutonNbClick';
import ToDoList from './components/ToDoList';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <header className="App-header" >
        <div className="App-frame" >
          <HelloMessage name="Renaud" surname="Levasseur" color= 'green'/>
          <HelloMessage name="Will" color= 'blue' />
        </div>
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <div className="App-frame" >
        <Timer/>
        <Timer delay="5000"/>
        </div>
        <div className="App-frame" >
        <BoutonNbClick/>
        </div>
        <div className="App-frame" >
        <ToDoList/>
        </div>
      </header>
    </div>
  );
}

export default App;


