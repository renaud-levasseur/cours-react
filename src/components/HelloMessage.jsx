import React from 'react';

class HelloMessage extends React.Component {
    render() {
        const {surname, name} = this.props
        const len = surname ? surname.length: "(error props famille undefined)"
        const divStyle = {
          color: this.props.color ,
        }
      return (
        <div style={divStyle}>
          Hello {name} {surname} Longueur Nom: {len}
        </div>
      );
    }
  }

  export default HelloMessage;




