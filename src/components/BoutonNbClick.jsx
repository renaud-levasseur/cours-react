import React from 'react';
import { Button } from 'react-bootstrap';

class Toggle extends React.Component {
    constructor(props) {
      super(props);
      this.state = {nbClick: 0};
      this.handleClick = this.handleClick.bind(this);
    }
    handleClick() {    
      this.setState(state => ({      
          nbClick: state.nbClick+1 
        })
      );  
    }
    render() {
      return (
        <Button onClick={this.handleClick}> 
            {this.state.nbClick}
        </Button>
      );
    }
}

export default Toggle;