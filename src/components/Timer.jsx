import React from 'react'

class Timer extends React.Component {
    constructor(props) {
      super(props);
      this.state = { seconds: 0 };
    }
  
    tick() {
      this.setState(state => ({
        seconds: state.seconds + 1
      }));
    }
  
    componentDidMount() {
      console.log("Component Did Mount")
      const delay= this.props.delay
      const realDelay = delay ? delay : 1000
      this.interval = setInterval(() => this.tick(), realDelay);
    }
  
    componentWillUnmount() {
      clearInterval(this.interval);
    }
  
    render() {
        const delay= this.props.delay
        //console.log("render", this.state)
      return (
        <div>
          Seconds: {this.state.seconds} Delay: {delay}
        </div>
      );
    }
  }

  export default Timer;